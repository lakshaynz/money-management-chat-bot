import peewee

db = peewee.SqliteDatabase('transactions.db')


class User(peewee.Model):
    user_id = peewee.CharField()
    class Meta:
        database = db

class Transaction(peewee.Model):
    amount = peewee.FloatField()
    category = peewee.CharField()
    description = peewee.CharField()
    user=peewee.ForeignKeyField(User, backref='transactions')

    class Meta:
        database = db

db.connect()
db.create_tables([User, Transaction])
