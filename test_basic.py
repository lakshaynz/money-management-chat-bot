import main

CHAT_ID = 123456789

def test_reply_to_spent():
    obj = main.spent("/spent 50, food, restaurant with friend",CHAT_ID)
    assert obj["response_text"] == "Transaction added: 50.0 NZD spent on food"
    assert obj["parsed"]["amount"] == 50.0
    assert obj["parsed"]["category"] == "food"
    assert obj["parsed"]["description"] == "restaurant with friend"
    obj = main.spent("/spent 169.55, shopping, samsung ssd",CHAT_ID)
    assert obj["response_text"] == "Transaction added: 169.55 NZD spent on shopping"
    assert obj["parsed"]["amount"] == 169.55
    assert obj["parsed"]["category"] == "shopping"
    assert obj["parsed"]["description"] == "samsung ssd"
    obj = main.spent("/spent 169.55, shopping",CHAT_ID)
    assert obj["response_text"] == "Transaction added: 169.55 NZD spent on shopping"
    assert obj["parsed"]["amount"] == 169.55
    assert obj["parsed"]["category"] == "shopping"
    assert obj["parsed"]["description"] == ""
    obj = main.spent("/spent 169.55",CHAT_ID)
    assert obj["response_text"] == "Please enter a category"
    obj = main.spent("/spent 169.55,",CHAT_ID)
    assert obj["response_text"] == "Please enter a category"
    obj = main.spent("/spent 169.55, shopping,",CHAT_ID)
    assert obj["response_text"] == "Transaction added: 169.55 NZD spent on shopping"
    assert obj["parsed"]["amount"] == 169.55
    assert obj["parsed"]["category"] == "shopping"
    assert obj["parsed"]["description"] == ""
    obj = main.spent("/spent 169.55, shopping, ",CHAT_ID)
    assert obj["response_text"] == "Transaction added: 169.55 NZD spent on shopping"
    assert obj["parsed"]["amount"] == 169.55
    assert obj["parsed"]["category"] == "shopping"
    assert obj["parsed"]["description"] == ""
    obj = main.spent("/spent 169.55, shopping, samsung ssd",CHAT_ID)
    assert obj["response_text"] == "Transaction added: 169.55 NZD spent on shopping"
    assert obj["parsed"]["amount"] == 169.55
    assert obj["parsed"]["category"] == "shopping"
    assert obj["parsed"]["description"] == "samsung ssd"
    

def test_get_last_transaction():
    main.spent("/spent 169.56, shopping, samsung ssd",CHAT_ID)
    main.spent("/spent 169.55, shopping, samsung ssd",CHAT_ID)
    obj = main.get_last_transaction(CHAT_ID)
    assert obj["response_text"] == "169.55 - shopping"
    main.spent("/spent 169.54, shopping, samsung ssd",CHAT_ID)
    obj2 = main.get_last_transaction(CHAT_ID)
    assert obj2["response_text"] == "169.54 - shopping"


def test_delete_last_transaction():
    main.spent("/spent 169.56, shopping, samsung ssd",CHAT_ID)
    main.spent("/spent 169.55, shopping, samsung ssd",CHAT_ID)
    obj = main.delete_last_transaction(CHAT_ID)
    assert obj["response_text"] == "169.55 - shopping deleted"
    obj2 = main.get_last_transaction(CHAT_ID)
    assert obj2["response_text"] == "169.56 - shopping"
    