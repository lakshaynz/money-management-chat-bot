FROM python:alpine
ENV PYTHONUNBUFFERED=TRUE
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
RUN pytest
CMD ["python", "server.py"]

