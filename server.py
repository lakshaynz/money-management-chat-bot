import http.server
import socketserver
import json
from main import handle_update
from set_webhook import set_webhook
PORT = 8000


# set_webhook()

class MyRequestHandler(http.server.BaseHTTPRequestHandler):
    
    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length)
        data = json.loads(body.decode('utf-8'))
        handle_update(data)
        self.send_response(200)
        self.send_header('Content-type', 'text/plain')
        self.end_headers()
        self.wfile.write(b'Received POST request')

with socketserver.TCPServer(("0.0.0.0", PORT), MyRequestHandler) as httpd:
    print("Server running on port", PORT)
    httpd.serve_forever()

