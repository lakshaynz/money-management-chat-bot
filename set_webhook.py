import requests
import os


BOT_TOKEN = os.environ.get("BOT_TOKEN")

WEBHOOK_URL = os.environ.get("TELEGRAM_WEBHOOK")

def set_webhook():
    url2= f'https://api.telegram.org/bot{BOT_TOKEN}/getWebhookInfo'
    response = requests.get(url2)
    print(response.json())
    response = requests.post(
        url=f"https://api.telegram.org/bot{BOT_TOKEN}/setWebhook",
        json={"url": WEBHOOK_URL},
    )

    if response.status_code == 200:
        print("Webhook successfully set!")
    else:
        print(f"Failed to set webhook: {response.text}")
