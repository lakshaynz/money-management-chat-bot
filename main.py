import requests
import os
import peewee

from models import Transaction

BOT_TOKEN = os.getenv('BOT_TOKEN')
URL = f'https://api.telegram.org/bot{BOT_TOKEN}/'


def spent(message,cid):
    message_split = message.split("/spent")
    message_split = message_split[1].split(",")
    message_split = [item.strip() for item in message_split]
    try:
        amount = float(message_split[0])
    except ValueError:
        return {"response_text": "Please enter a valid amount"}
    try:
        category = message_split[1]
        if category == "":
            raise IndexError
    except IndexError:
        return {"response_text": "Please enter a category"}
    try:
        description = message_split[2]
    except IndexError:
        description = ""
    Transaction.create(amount=amount, category=category, description=description, user=cid)
    return {"response_text": f"Transaction added: {amount} NZD spent on {category}", "parsed": {"amount": amount, "category": category, "description": description}}

def handle_update(data):
    chat_id = data['message']['chat']['id']
    message_text = data['message']['text']
    if message_text.startswith('/spent'):
        response = spent(message_text, chat_id)
    elif message_text.startswith('/budget'):
        response = {'response_text': 'budget not implemented yet'}
    else:
        response = {'response_text': 'please use /spent or /budget'}
    send_message(chat_id, response['response_text'])

def send_message(chat_id, text):
    response = requests.post(URL + 'sendMessage', {'chat_id': chat_id, 'text': text})
    return response.json()

def print_to_console(data):
    for key, value in data.items():
        print(key, value)
        if isinstance(value, dict):
            handle_update(value)


def delete_last_transaction(user_id):
    transaction = Transaction.select().where(Transaction.user_id == user_id).order_by(Transaction.id.desc()).get()
    response_text = f"{transaction.amount} - {transaction.category} deleted"
    transaction.delete_instance()
    return {"response_text": response_text}


def get_last_transaction(user_id):
    transaction = Transaction.select().where(Transaction.user_id == user_id).order_by(Transaction.id.desc()).get()
    return {"response_text": f"{transaction.amount} - {transaction.category}"}


def add_transaction_to_db(amount, category, description,user_id):
    transaction = Transaction(amount=amount, category=category, description=description, user_id=user_id)
    transaction.save()
